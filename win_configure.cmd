@echo off
set /p name="Enter the project name: "
set /p ts="Enter the timestamp field number (counting from 0): "
set /p lat="Enter the latitude field number (counting from 0): "
set /p lon="Enter the longitude field number (counting from 0): "
set /p alt="Enter the altitude field number (counting from 0): "

(
	echo %name%
	echo %ts%
	echo %lat%
	echo %lon%
	echo %alt%
) > .project_config
@echo on
