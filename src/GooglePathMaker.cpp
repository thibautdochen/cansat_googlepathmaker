#include "GooglePathMaker.h"
#include <iostream>
#include <string>
#include <vector>
#include <fstream> 
#include <sstream>
#include <assert.h>

void GooglePathMaker::begin(int ts, int lat, int lon, int alt){
	getInputFile();

	ifstream tmpStream;
	tmpStream.open(inputFile);
	string tmpInput;
	while(getline(tmpStream, tmpInput)){
		rawInput.push_back(tmpInput);
	}
	tmpStream.close();
	
	for(int i=0;i<rawInput.size();i++){
		vector<string> splittedInput;
		mySplit(rawInput[i], splittedInput);
		extractData(splittedInput, ts, lat, lon, alt);
	}
}

void GooglePathMaker::extractData(vector<string> rawData, int ts, int lat, int lon, int alt){
	if(!isdigit(rawData[0][0])) return;
	
	record currentRecord;

	currentRecord.ts = rawData[ts/*0*/];
	currentRecord.GPS_Latitude = rawData[lat/*2*/];
	currentRecord.GPS_Longitude = rawData[lon/*3*/];
	currentRecord.altitude = rawData[alt/*7*/];
	
	data.push_back(currentRecord);
}


void GooglePathMaker::run(long long takeoff, long long landing, string team){
	string tabs = "          ";
	ifstream kml_part;
	ofstream finalKML;
	getOutputFile();
	finalKML.open(outputFile);
	
	kml_part.open("kml/path_part0.kml");
	transferContent(kml_part, finalKML);
	kml_part.close();
	
	finalKML << team;
	
	kml_part.open("kml/path_part1.kml");
	transferContent(kml_part, finalKML);
	kml_part.close();
	
	int index=0;
	
	for(int i=index;stod(data[i].ts)<takeoff && i < data.size();i++){
		if(data[i].GPS_Longitude[0]!='0'){
			finalKML << tabs << data[i].GPS_Longitude << "," << data[i].GPS_Latitude << "," << data[i].altitude << "\n";
		}
		index=i;
	}
	
	
	kml_part.open("kml/path_part2.kml");
	transferContent(kml_part, finalKML);
	kml_part.close();
	
	for(int i=index;stod(data[i].ts)<landing && i < data.size();i++){
		if(data[i].GPS_Longitude[0]!='0'){
			finalKML << tabs << data[i].GPS_Longitude << "," << data[i].GPS_Latitude << "," << data[i].altitude << "\n";
		}
		index=i;
	}
	
	kml_part.open("kml/path_part3.kml");
	transferContent(kml_part, finalKML);
	kml_part.close();
	
	for(int i=index;i<data.size();i++){
		if(data[i].GPS_Longitude[0]!='0'){
			finalKML << tabs << data[i].GPS_Longitude << "," << data[i].GPS_Latitude << "," << data[i].altitude << "\n";
		}
	}
	
	kml_part.open("kml/path_part4.kml");
	transferContent(kml_part, finalKML);
	kml_part.close();
	
	finalKML.close();
}


double GooglePathMaker::stod(string s){
	stringstream convert(s);
	double returnVal = 0;
	convert >> returnVal;
	return returnVal;
}


void GooglePathMaker::transferContent(ifstream &in, ofstream &out){
	string currLine;
	while(getline(in, currLine)){
		out << currLine << "\n";
	}
}

void GooglePathMaker::getInputFile(){
	cout << "Input file: ";
	string file;
	cin >> file;
	file = inputFileDir + file;
	assert(verifyFile(file));
	inputFile = file;
}

void GooglePathMaker::getOutputFile(){
	cout << "Output file: ";
	string file;
	cin >> file;
	file = outputFileDir + file;
	outputFile = file;
}

bool GooglePathMaker::verifyFile(string dir){
	ifstream f(dir.c_str());
	return f.good();
}

void GooglePathMaker::mySplit(string s, vector<string> &output, char splitter){
	int beginSubStr = 0;
	int lenSubStr = 0;
	for (int i=0;i<s.length();i++){
		if(s[i]!=splitter){
			lenSubStr++;
		}
		else{
			output.push_back(s.substr(beginSubStr, lenSubStr));
			beginSubStr = i+1;
			lenSubStr = 0;
		}
	}
	output.push_back(s.substr(beginSubStr, s.length()-beginSubStr));
}
