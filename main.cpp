#include "src/GooglePathMaker.h"


#include <iostream>

int main(){
	GooglePathMaker googlePathMaker;
	
	ifstream config(".project_config");
	string team; int ts, lat, lon, alt;
	config >> team >> ts >> lat >> lon >> alt;

	googlePathMaker.begin(ts, lat, lon, alt);

	long long takeoff, landing;
	cout << "Takeoff timestamp: ";
	cin >> takeoff;
	cout << "Landing timestamp: ";
	cin >> landing;
	googlePathMaker.run(takeoff, landing, team);
}
